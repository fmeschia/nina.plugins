﻿using Newtonsoft.Json;
using NINA.Sequencer.Conditions;
using NINA.Sequencer.SequenceItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NINA.Plugins.DebugInstructions.Conditions {
    [ExportMetadata("Name", "While (false)")]
    [ExportMetadata("Description", "This condition is always true")]
    [ExportMetadata("Icon", "CancelCircledSVG")]
    [ExportMetadata("Category", "DebugInstructions")]
    [Export(typeof(ISequenceCondition))]
    [JsonObject(MemberSerialization.OptIn)]
    public class AlwaysFalseCondition : SequenceCondition {
        public override bool Check(ISequenceItem previousItem, ISequenceItem nextItem) {
            return false;
        }

        public override object Clone() {
            return new AlwaysFalseCondition() {
                Icon = Icon,
                Name = Name,
                Category = Category,
                Description = Description
            };
        }
    }
}
