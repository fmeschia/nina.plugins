﻿using Newtonsoft.Json;
using NINA.Core.Model;
using NINA.Sequencer.SequenceItem;
using NINA.Sequencer.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NINA.Plugins.DebugInstructions.Instructions {
    
    [ExportMetadata("Name", "Failed Validation Instruction")]
    [ExportMetadata("Description", "This instruction will always fail the validation")]
    [ExportMetadata("Icon", "SkipSVG")]
    [ExportMetadata("Category", "DebugInstructions")]
    [Export(typeof(ISequenceItem))]
    [JsonObject(MemberSerialization .OptIn)]
    public class FailedValidationInstruction : SequenceItem, IValidatable {
        public IList<string> Issues => new List<string> { "Validation for this instruction will always fail" };

        public override object Clone() {
            return new FailedValidationInstruction() {
                Icon = Icon,
                Name = Name,
                Category = Category,
                Description = Description
            };
        }

        public override Task Execute(IProgress<ApplicationStatus> progress, CancellationToken token) {
            throw new Exception("This instruction should never run");
        }

        public bool Validate() {
            return false;
        }
    }
}
