﻿using Newtonsoft.Json;
using NINA.Core.Model;
using NINA.Sequencer.SequenceItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NINA.Plugins.DebugInstructions.Instructions {
    [ExportMetadata("Name", "Successful Instruction")]
    [ExportMetadata("Description", "This instruction will always be successful")]
    [ExportMetadata("Icon", "CheckedCircledSVG")]
    [ExportMetadata("Category", "DebugInstructions")]
    [Export(typeof(ISequenceItem))]
    [JsonObject(MemberSerialization.OptIn)]
    public class SuccessfulInstruction : SequenceItem {
        public override object Clone() {
            return new SuccessfulInstruction() {
                Icon = Icon,
                Name = Name,
                Category = Category,
                Description = Description
            };
        }

        public override Task Execute(IProgress<ApplicationStatus> progress, CancellationToken token) {
            return Task.CompletedTask;
        }
    }
}
