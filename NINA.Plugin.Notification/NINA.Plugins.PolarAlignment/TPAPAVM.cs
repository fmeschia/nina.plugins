﻿using Accord.Math;
using Accord.Math.Geometry;
using NINA.Astrometry;
using NINA.Core.Model;
using NINA.Core.Utility;
using NINA.Image.ImageAnalysis;
using NINA.Image.Interfaces;
using NINA.PlateSolving;
using NINA.Profile.Interfaces;
using NINA.WPF.Base.Behaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace NINA.Plugins.PolarAlignment {
    public class TPAPAVM : BaseINPC {

        public TPAPAVM(IProfileService profileService) {
            this.profileService = profileService;
            Status = new ApplicationStatus();
            Step = string.Empty;
            DragMoveCommand = new RelayCommand(obj => {
                var dragResult = (DragResult)obj;
                if (dragResult.Mode == DragMode.Move) {
                    ErrorDetail.Shift(dragResult.Delta);
                }
            });
            LeftMouseButtonDownCommand = new AsyncCommand<bool>(async obj => {if (obj != null) { await SelectNewReferenceStar((Point)obj); } return true; });
        }

        private SemaphoreSlim selectNewStarLock = new SemaphoreSlim(1);
        private SemaphoreSlim starDetectionLock = new SemaphoreSlim(1);

        public async Task SelectNewReferenceStar(Point p) {
            await selectNewStarLock.WaitAsync();
            try {
                ReferenceStar = await GetClosestStarPosition(Image, p, default, new CancellationToken());
                ReferenceStarCoordinates = PolarErrorDetermination.CurrentReferenceFrame.Coordinates.Shift(ReferenceStar.X - Center.X, ReferenceStar.Y - Center.Y, PolarErrorDetermination.CurrentReferenceFrame.Orientation, ArcsecPerPix, ArcsecPerPix);

                CalculateErrorDetails();
            } finally {
                selectNewStarLock.Release();
            }
        }


        public async Task UpdateDetails(PlateSolveResult psr) {
            PolarErrorDetermination.CurrentReferenceFrame = psr;
           var currentCenter = PolarErrorDetermination.CurrentReferenceFrame;

            if ((psr.Coordinates - currentCenter.Coordinates).Distance.ArcSeconds > ArcsecPerPix) {
                // To minimize projection errors, try to re-acquire the same star from star detection instead of just projecting
                var p = ReferenceStarCoordinates.XYProjection(currentCenter.Coordinates, Center, ArcsecPerPix, ArcsecPerPix, currentCenter.Orientation);
                await SelectNewReferenceStar(p);
            } else {
                currentCenter = psr;
                ReferenceStar = ReferenceStarCoordinates.XYProjection(currentCenter.Coordinates, Center, ArcsecPerPix, ArcsecPerPix, currentCenter.Orientation);
                CalculateErrorDetails();
            }
        }

        private void CalculateErrorDetails() {
            var currentCenter = PolarErrorDetermination.CurrentReferenceFrame;

            var originPixel = PolarErrorDetermination.InitialReferenceFrame.Coordinates.XYProjection(currentCenter.Coordinates, Center, ArcsecPerPix, ArcsecPerPix, currentCenter.Orientation);


            var pointShift = Center - originPixel;
            originPixel = originPixel + pointShift * 2;

            var destinationAltAz = PolarErrorDetermination.GetDestinationCoordinates(-PolarErrorDetermination.InitialMountAxisAzimuthError.Degree, -PolarErrorDetermination.InitialMountAxisAltitudeError.Degree).Transform(Epoch.J2000);
            var destinationPixel = destinationAltAz.XYProjection(currentCenter.Coordinates, Center, ArcsecPerPix, ArcsecPerPix, currentCenter.Orientation);
            destinationPixel = destinationPixel + pointShift * 2;


            // Azimuth
            var originalAzimuthAltAz = PolarErrorDetermination.GetDestinationCoordinates(-PolarErrorDetermination.InitialMountAxisAzimuthError.Degree, 0).Transform(Epoch.J2000);
            var originalAzimuthPixel = originalAzimuthAltAz.XYProjection(currentCenter.Coordinates, Center, ArcsecPerPix, ArcsecPerPix, currentCenter.Orientation);
            originalAzimuthPixel = originalAzimuthPixel + pointShift * 2;

            var lineOriginToAzimuth = Line.FromPoints(ToAccordPoint(originPixel), ToAccordPoint(originalAzimuthPixel));

            var correctedAzimuthLine = Line.FromSlopeIntercept(lineOriginToAzimuth.Slope, (float)(Center.Y - lineOriginToAzimuth.Slope * Center.X));

            var lineAzimuthToDestination = Line.FromPoints(ToAccordPoint(originalAzimuthPixel), ToAccordPoint(destinationPixel));

            var correctedAzimuthPixel = lineAzimuthToDestination.GetIntersectionWith(correctedAzimuthLine); // az corrected position

            var correctedAzimuthDistance = correctedAzimuthPixel.Value.DistanceTo(ToAccordPoint(Center)); // az corrected
            var originalAzimuthDistance = ToAccordPoint(originalAzimuthPixel).DistanceTo(ToAccordPoint(originPixel)); // az orig

            // Altitude
            var originalAltitudeAltAz = PolarErrorDetermination.GetDestinationCoordinates(0, -PolarErrorDetermination.InitialMountAxisAltitudeError.Degree).Transform(Epoch.J2000);
            var originalAltitudePixel = originalAltitudeAltAz.XYProjection(currentCenter.Coordinates, Center, ArcsecPerPix, ArcsecPerPix, currentCenter.Orientation);
            originalAltitudePixel = originalAltitudePixel + pointShift * 2;

            var lineOriginToAltitude = Line.FromPoints(ToAccordPoint(originPixel), ToAccordPoint(originalAltitudePixel));
            
            var correctedAltitudeLine = Line.FromSlopeIntercept(lineOriginToAltitude.Slope, (float)(Center.Y - lineOriginToAltitude.Slope * Center.X));

            var lineAltitudeToDestination = Line.FromPoints(ToAccordPoint(originalAltitudePixel), ToAccordPoint(destinationPixel));
            
            var correctedAltitudePixel = lineAltitudeToDestination.GetIntersectionWith(correctedAltitudeLine); // alt corrected pixel

            var correctedAltitudeDistance = correctedAltitudePixel.Value.DistanceTo(ToAccordPoint(Center)); // alt corrected
            var originalAltitudeDistance = ToAccordPoint(originalAltitudePixel).DistanceTo(ToAccordPoint(originPixel)); // az corrected


            // Check if sign needs to be reversed

            // Azimuth
            var originalAzimuthDirection = ToAccordPoint(destinationPixel) - ToAccordPoint(originalAltitudePixel);
            var correctedAzimuthDirection = ToAccordPoint(destinationPixel) - correctedAltitudePixel.Value;
            // When dot product is positive, the angle between both vectors is smaller than 90°
            var azimuthSameDirection = (originalAzimuthDirection.X * correctedAzimuthDirection.X + originalAzimuthDirection.Y * correctedAzimuthDirection.Y) > 0;

            var azSign = 1;
            if (!azimuthSameDirection) {
                azSign = -1;
            }

            // Altitude
            var originalAltitudeDirection = ToAccordPoint(destinationPixel) - ToAccordPoint(originalAzimuthPixel);
            var correctedAltitudeDirection = ToAccordPoint(destinationPixel) - correctedAzimuthPixel.Value;
            // When dot product is positive, the angle between both vectors is smaller than 90°
            var AltitudeSameDirection = (originalAltitudeDirection.X * correctedAltitudeDirection.X + originalAltitudeDirection.Y * correctedAltitudeDirection.Y) > 0;

            var altSign = 1;
            if (!AltitudeSameDirection) {
                altSign = -1;
            }

            // Error determination

            PolarErrorDetermination.CurrentMountAxisAzimuthError = Angle.ByDegree(PolarErrorDetermination.InitialMountAxisAzimuthError.Degree * (azSign * correctedAzimuthDistance / originalAzimuthDistance));
            PolarErrorDetermination.CurrentMountAxisAltitudeError = Angle.ByDegree(PolarErrorDetermination.InitialMountAxisAltitudeError.Degree * (altSign * correctedAltitudeDistance / originalAltitudeDistance));
            PolarErrorDetermination.CurrentMountAxisTotalError = Angle.ByDegree(Accord.Math.Tools.Hypotenuse(PolarErrorDetermination.CurrentMountAxisAltitudeError.Degree, PolarErrorDetermination.CurrentMountAxisAzimuthError.Degree));
            
            var errorDetail = new ErrorDetail(Center, ToPoint(correctedAltitudePixel.Value), ToPoint(correctedAzimuthPixel.Value), destinationPixel);
            
            //errorDetail.Shift(pointShift);
            errorDetail.Shift(new System.Windows.Vector(ReferenceStar.X - Center.X, ReferenceStar.Y - Center.Y));
            ErrorDetail = errorDetail;

            var errorDetail2 = new ErrorDetail(originPixel, originalAltitudePixel, originalAzimuthPixel, destinationPixel);
            errorDetail2.Shift(new System.Windows.Vector(ReferenceStar.X - Center.X, ReferenceStar.Y - Center.Y)); 
            ErrorDetail2 = errorDetail2;
        }

        public Accord.Point ToAccordPoint(Point p) {
            return new Accord.Point((float)p.X, (float)p.Y);
        }

        public Point ToPoint(Accord.Point p) {
            return new Point(p.X, p.Y);
        }

        public async Task<Point> GetClosestStarPosition(IRenderedImage image, Point reference, IProgress<ApplicationStatus> progress, CancellationToken token) {
            var detection = await GetStarDetection(image, progress, token);

            var closestStarToReference = detection.StarList
                .GroupBy(p => Math.Pow(reference.X - p.Position.X, 2) + Math.Pow(reference.Y - p.Position.Y, 2))
                .OrderBy(p => p.Key)
                .FirstOrDefault()?.FirstOrDefault();
            return new Point(closestStarToReference.Position.X, closestStarToReference.Position.Y);
        }


        private KeyValuePair<int, StarDetection> starDetection;
        private async Task<StarDetection> GetStarDetection(IRenderedImage image, IProgress<ApplicationStatus> progress, CancellationToken token) {
            await starDetectionLock.WaitAsync();
            try {
                if(starDetection.Value == null || starDetection.Key != image.RawImageData.MetaData.Image.Id) { 
                    var detection = new StarDetection(image, image.Image.Format, profileService.ActiveProfile.ImageSettings.StarSensitivity, profileService.ActiveProfile.ImageSettings.NoiseReduction);                
                    detection.NumberOfAFStars = 500;
                    await detection.DetectAsync(progress, token);
                    starDetection = new KeyValuePair<int, StarDetection>(image.RawImageData.MetaData.Image.Id, detection);
                }
                return starDetection.Value;
            } finally {
                starDetectionLock.Release();
            }
        }

        private PolarErrorDetermination polarErrorDetermination;
        public PolarErrorDetermination PolarErrorDetermination {
            get => polarErrorDetermination;
            set {
                polarErrorDetermination = value;
                RaisePropertyChanged();
            }
        }

        public ICommand DragMoveCommand { get; }
        public ICommand LeftMouseButtonDownCommand { get; }

        private string step;
        public string Step { get => step; set { step = value; RaisePropertyChanged(); } }

        private ApplicationStatus status;
        private IProfileService profileService;

        public ApplicationStatus Status { get => status; set { status = value; RaisePropertyChanged(); } }

        public bool Northern {
            get => Latitude.Degree > 0;
        }

        private IRenderedImage image;

        private ErrorDetail erorLines; 
        public ErrorDetail ErrorDetail { get => erorLines; set { erorLines = value; RaisePropertyChanged(); } }

        private ErrorDetail erorLines2;
        public ErrorDetail ErrorDetail2 { get => erorLines2; set { erorLines2 = value; RaisePropertyChanged(); } }
        public Angle Latitude {
            get => Angle.ByDegree(profileService.ActiveProfile.AstrometrySettings.Latitude);
        }
        public Angle Longitude {
            get => Angle.ByDegree(profileService.ActiveProfile.AstrometrySettings.Longitude);
        }
        public IRenderedImage Image { get => image; internal set { image = value; RaisePropertyChanged(); } }

        public Point ReferenceStar { get; private set; }
        public Coordinates ReferenceStarCoordinates { get; internal set; }
        public Point Center { get; internal set; }
        public double ArcsecPerPix { get; internal set; }
    }

    public sealed class TPAPAStep {
        public const string MovingToTarget = "Slewing To Target";
        public const string FirstPoint = "Retrieving first measurement point";
        public const string SecondPoint = "Retrieving second measurement point";
        public const string FinalPoint = "Retrieving final measurement point";
        public const string Adjustment = "Please adjust Altitude / Azimuth";
    }

    public class ErrorDetail : BaseINPC {
        
        public ErrorDetail(Point origin, Point altitude, Point azimuth, Point total) {
            Origin = origin;
            Altitude = altitude;
            Azimuth = azimuth;
            Total = total;
            RaisePropertyChanged(nameof(Rectangle));
        }

        public Point Origin { get; set; }
        public Point Altitude { get; set; }
        public Point Azimuth { get; set; }
        public Point Total { get; set; }

        public PointCollection Rectangle { 
            get => new PointCollection  {
                Origin,
                Altitude,
                Total,
                Azimuth
             };
        }

        

        public void Shift(System.Windows.Vector delta) {
            Origin = new Point(Origin.X + delta.X, Origin.Y + delta.Y);
            Altitude = new Point(Altitude.X + delta.X, Altitude.Y + delta.Y);
            Azimuth = new Point(Azimuth.X + delta.X, Azimuth.Y + delta.Y);
            Total = new Point(Total.X + delta.X, Total.Y + delta.Y);
            RaiseAllPropertiesChanged();
        }
    }

    public class ErrorLines : BaseINPC {


        private ErrorLine total;
        public ErrorLine Total { get => total; set { total = value; RaisePropertyChanged(); } }


        private ErrorLine altitude;
        public ErrorLine Altitude { get => altitude; set { altitude = value; RaisePropertyChanged(); } }


        private ErrorLine azimuth;
        public ErrorLine Azimuth { get => azimuth; set { azimuth = value; RaisePropertyChanged(); } }


        private Point delta;
        public Point Delta { get => delta; set { delta = value; RaisePropertyChanged(); } }
    }

    public class ErrorLine {
        public ErrorLine(Point start, Point end) {
            Start = start;
            End = end;
        }

        public Point Start { get; }
        public Point End { get; }
    }


    public class PolarErrorDetermination : BaseINPC {
        public PolarErrorDetermination(PlateSolveResult referenceFrame, Position position1, Position position2, Position position3, Angle latitude, Angle longitude) {
            Latitude = latitude;
            Longitude = longitude;

            InitialReferenceFrame = referenceFrame;
            FirstPosition = position1;
            SecondPosition = position2;
            ThirdPosition = position3;

            var planeVector = Vector3.DeterminePlaneVector(FirstPosition.Vector, SecondPosition.Vector, ThirdPosition.Vector);

            if ((Northern && planeVector.X < 0) || (!Northern && planeVector.X > 0)) {
                // Flip vector if pointing to the wrong direction
                planeVector = new Vector3(-planeVector.X, -planeVector.Y, -planeVector.Z);
            }


            InitialMountAxisErrorPosition = new Position(planeVector, Latitude, Longitude);

            CalculateMountAxisError();
        }

        public Angle Latitude { get; }
        public Angle Longitude { get; }

        public bool Northern {
            get => Latitude.Degree > 0;
        }

        public PlateSolveResult InitialReferenceFrame { get; }
        public Position FirstPosition { get;  }
        public Position SecondPosition { get;  }
        public Position ThirdPosition { get;  }

        public Position InitialMountAxisErrorPosition { get;  }

        public Angle InitialMountAxisAltitudeError { get; private set; }
        public Angle InitialMountAxisAzimuthError { get; private set; }
        public Angle InitialMountAxisTotalError { get; private set; }
        public PlateSolveResult CurrentReferenceFrame { get; set; }

        private Angle currentMountAxisAltitudeError;
        public Angle CurrentMountAxisAltitudeError {
            get => currentMountAxisAltitudeError;
            set {
                currentMountAxisAltitudeError = value;
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(CurrentMountAxisAltitudeErrorDirection));
            }
        }
        private Angle currentMountAxisAzimuthError;
        public Angle CurrentMountAxisAzimuthError {
            get => currentMountAxisAzimuthError;
            set {
                currentMountAxisAzimuthError = value;
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(CurrentMountAxisAzimuthErrorDirection));
            }
        }
        private Angle currentMountAxisTotalError;
        public Angle CurrentMountAxisTotalError {
            get => currentMountAxisTotalError;
            set {
                currentMountAxisTotalError = value;
                RaisePropertyChanged();
            }
        }

        public string CurrentMountAxisAltitudeErrorDirection { 
            get {
                if (CurrentMountAxisAltitudeError.Degree > 0) {
                    if (Northern) {
                        return "Move down 🠗";
                    } else {
                        return "Move up 🠕";
                    }
                } else if (CurrentMountAxisAltitudeError.Degree < 0) {
                    if (Northern) {
                        return "Move up 🠕";
                    } else {
                        return "Move down 🠗";
                    }
                } else {
                    return string.Empty;
                }
            }
        }
        public string CurrentMountAxisAzimuthErrorDirection { 
            get {
                if (CurrentMountAxisAzimuthError.Degree > 0) {
                if (Northern) {
                    return "Move left 🠔";
                } else {
                        return "Move right 🠖";
                }
            } else if (CurrentMountAxisAzimuthError.Degree < 0) {
                if (Northern) {
                        return "Move right 🠖";
                } else {
                        return "Move left 🠔";
                }
            } else {
                return string.Empty;
            }
            }
        }

        /// <summary>
        /// Calculate the error based on the measured telescope axis compared to the polar axis
        /// Polar axis = Azimuth 0 | Altitude = Latitude
        /// </summary>
        /// <returns></returns>
        private void CalculateMountAxisError() {
            var altitudeError = Angle.Zero;
            var azimuthError = Angle.Zero;
            if (Northern) {
                altitudeError = InitialMountAxisErrorPosition.Topocentric.Altitude - Latitude;
                azimuthError = InitialMountAxisErrorPosition.Topocentric.Azimuth;
            } else {
                altitudeError = InitialMountAxisErrorPosition.Topocentric.Altitude + Latitude;
                azimuthError = InitialMountAxisErrorPosition.Topocentric.Azimuth + Angle.ByDegree(180);
            }

            if(azimuthError.Degree > 180) {
                azimuthError = Angle.ByDegree(azimuthError.Degree - 360);
            }
            if (azimuthError.Degree < -180) {
                azimuthError = Angle.ByDegree(azimuthError.Degree + 360);
            }

            InitialMountAxisAltitudeError = altitudeError;
            InitialMountAxisAzimuthError = azimuthError;
            InitialMountAxisTotalError = Angle.ByDegree(Accord.Math.Tools.Hypotenuse(InitialMountAxisAltitudeError.Degree, InitialMountAxisAzimuthError.Degree));

            CurrentMountAxisAltitudeError = altitudeError;
            CurrentMountAxisAzimuthError = azimuthError;
            CurrentMountAxisTotalError = Angle.ByDegree(Accord.Math.Tools.Hypotenuse(InitialMountAxisAltitudeError.Degree, InitialMountAxisAzimuthError.Degree));
            CurrentReferenceFrame = InitialReferenceFrame;
        }

        public TopocentricCoordinates GetDestinationCoordinates(double azAngle, double altAngle) {
            var referenceTopo = InitialReferenceFrame.Coordinates.Transform(Latitude, Longitude);

            var vRef = Vector3.CoordinatesToUnitVector(referenceTopo);

            var azRotation = Angle.ByDegree(azAngle);
            var altRotation = Angle.ByDegree(altAngle);

            //First rotate by azimuth, then from the point at azimuth rotate further by altitude to get to the final position
            var azDest = Vector3.RotateByRodrigues(vRef, new Vector3(0, 0, 1), azRotation);
            var rotatedAltAxis = Vector3.RotateByRodrigues(new Vector3(0, 1, 0), new Vector3(0, 0, 1), azRotation);

            var finalDest = Vector3.RotateByRodrigues(azDest, rotatedAltAxis, altRotation); //combination of first az then applied alt

            return finalDest.ToTopocentric(Latitude, Longitude);
        }

    }
    
    public class Position {
        public Position(Coordinates coordinates, Angle latitude, Angle longitude) {     
            Topocentric = coordinates.Transform(latitude, longitude);
            Vector = Vector3.CoordinatesToUnitVector(Topocentric);
        }
        public Position(Vector3 vector, Angle latitude, Angle longitude) {
            Topocentric = vector.ToTopocentric(latitude, longitude);
            Vector = vector;
        }

        public Position(TopocentricCoordinates coordinates) {            
            Topocentric = coordinates;
            Vector = Vector3.CoordinatesToUnitVector(Topocentric);
        }

        public Vector3 Vector { get; }
        public TopocentricCoordinates Topocentric { get; }
    }
}
