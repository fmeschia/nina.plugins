﻿using NINA.Astrometry;
using NINA.Core.Model;
using NINA.Core.Model.Equipment;
using NINA.Core.Utility;
using NINA.Equipment.Equipment.MyCamera;
using NINA.Equipment.Interfaces.Mediator;
using NINA.Equipment.Interfaces.ViewModel;
using NINA.Equipment.Model;
using NINA.PlateSolving;
using NINA.Profile.Interfaces;
using NINA.WPF.Base.Interfaces.Mediator;
using NINA.WPF.Base.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace NINA.Plugins.PolarAlignment.Dockables {
    [Export(typeof(IDockableVM))]
    public class DockablePolarAlignmentVM : DockableVM {
        private IApplicationStatusMediator applicationStatusMediator;
        private ICameraMediator cameraMediator;
        private IImagingMediator imagingMediator;
        private IFilterWheelMediator fwMediator;
        private ITelescopeMediator telescopeMediator;
        private double moveRate;
        private double searchRadius;
        private int targetDistance;
        private bool eastDirection;
        private TPAPAVM tPAPAVM;
        private CancellationTokenSource executeCTS;

        [ImportingConstructor]
        public DockablePolarAlignmentVM(IProfileService profileService, IApplicationStatusMediator applicationStatusMediator, ICameraMediator cameraMediator, IImagingMediator imagingMediator, IFilterWheelMediator fwMediator, ITelescopeMediator telescopeMediator) : base(profileService) {
            Title = "Three Point Polar Alignment";

            var dict = new ResourceDictionary();
            dict.Source = new Uri("NINA.Plugins.PolarAlignment;component/Options.xaml", UriKind.RelativeOrAbsolute);
            ImageGeometry = (System.Windows.Media.GeometryGroup)dict["ThreePointsSVG"];
            ImageGeometry.Freeze();

            TPAPAVM = new TPAPAVM(profileService);

            this.profileService = profileService;
            this.cameraMediator = cameraMediator;
            this.imagingMediator = imagingMediator;
            this.fwMediator = fwMediator;
            this.telescopeMediator = telescopeMediator;
            this.applicationStatusMediator = applicationStatusMediator;

            Gain = profileService.ActiveProfile.PlateSolveSettings.Gain;
            Offset = -1;
            ExposureTime = profileService.ActiveProfile.PlateSolveSettings.ExposureTime;
            Binning = new BinningMode(profileService.ActiveProfile.PlateSolveSettings.Binning, profileService.ActiveProfile.PlateSolveSettings.Binning);

            EastDirection = Properties.Settings.Default.DefaultEastDirection;
            MoveRate = Properties.Settings.Default.DefaultMoveRate;
            TargetDistance = Properties.Settings.Default.DefaultTargetDistance;
            SearchRadius = Properties.Settings.Default.DefaultSearchRadius;

            CameraInfo = this.cameraMediator.GetInfo();

            if (Northern) {
                Coordinates = new InputTopocentricCoordinates(new TopocentricCoordinates(Angle.ByDegree(Properties.Settings.Default.DefaultAzimuthOffset), Latitude + Angle.ByDegree(Properties.Settings.Default.DefaultAltitudeOffset), Latitude, Longitude));
            } else {
                Coordinates = new InputTopocentricCoordinates(new TopocentricCoordinates(Angle.ByDegree(Properties.Settings.Default.DefaultAzimuthOffset), Angle.ByDegree(Math.Abs(Latitude.Degree)) + Angle.ByDegree(Properties.Settings.Default.DefaultAltitudeOffset), Latitude, Longitude));
            }

            ExecuteCommand = new AsyncCommand<bool>(async () => { using (executeCTS = new CancellationTokenSource()) { return await Execute(new Progress<ApplicationStatus>(p => Status = p), executeCTS.Token); } }, (object o) => { return telescopeMediator.GetInfo().Connected == true && cameraMediator.GetInfo().Connected == true; });
            CancelExecuteCommand = new RelayCommand((object o) => { try { executeCTS?.Cancel(); } catch (Exception) { } });
        }

        private ApplicationStatus _status;

        public ApplicationStatus Status {
            get {
                return _status;
            }
            set {
                _status = value;
                if (string.IsNullOrWhiteSpace(_status.Source)) {
                    _status.Source = "Three Point Polar Alignment";
                }

                RaisePropertyChanged();

                applicationStatusMediator.StatusUpdate(_status);
            }
        }

        public IAsyncCommand ExecuteCommand { get; }

        public ICommand CancelExecuteCommand { get; }

        public override bool IsTool { get; } = true;

        public TPAPAVM TPAPAVM {
            get => tPAPAVM;
            private set {
                tPAPAVM = value;
                RaisePropertyChanged();
            }
        }

        public InputTopocentricCoordinates Coordinates { get; set; }

        public double MoveRate {
            get => moveRate;
            set {
                moveRate = value;
                RaisePropertyChanged();
            }
        }

        public double SearchRadius {
            get => searchRadius;
            set {
                searchRadius = value;
                RaisePropertyChanged();
            }
        }

        public bool EastDirection {
            get => eastDirection;
            set {
                eastDirection = value;
                RaisePropertyChanged();
            }
        }

        public int TargetDistance {
            get => targetDistance;
            set {
                targetDistance = value;
                RaisePropertyChanged();
            }
        }
        private ApplicationStatus GetStatus(string status) {
            return new ApplicationStatus { Source = "Polar Alignment", Status = status };
        }

        public async Task<bool> Execute(IProgress<ApplicationStatus> externalProgress, CancellationToken token) {
            try {
                using (var localCTS = CancellationTokenSource.CreateLinkedTokenSource(token)) {
                    IProgress<ApplicationStatus> progress = new Progress<ApplicationStatus>(p => { TPAPAVM.Status = p; externalProgress?.Report(p); });

                    TPAPAVM.PolarErrorDetermination = null;
                    TPAPAVM.Image = null;
                    TPAPAVM.Step = TPAPAStep.MovingToTarget;
                    Logger.Info($"Slewing to initial position {Coordinates.Coordinates}");
                    await telescopeMediator.SlewToCoordinatesAsync(Coordinates.Coordinates, localCTS.Token);
                    telescopeMediator.SetTrackingEnabled(true);

                    TPAPAVM.Step = TPAPAStep.FirstPoint;

                    var solve1 = await Solve(TPAPAVM, progress, localCTS.Token);

                    var position1 = new Position(solve1.Coordinates, Latitude, Longitude);

                    Logger.Info($"First measurement point {solve1.Coordinates} - Vector: {position1.Vector}");

                    TPAPAVM.Step = TPAPAStep.SecondPoint;

                    await MoveToNextPoint(progress, localCTS.Token);
                    var solve2 = await Solve(TPAPAVM, progress, localCTS.Token);

                    var position2 = new Position(solve2.Coordinates, Latitude, Longitude);

                    Logger.Info($"Second measurement point {solve2.Coordinates} - Vector: {position2.Vector}");

                    TPAPAVM.Step = TPAPAStep.FinalPoint;

                    await MoveToNextPoint(progress, localCTS.Token);
                    var solve3 = await Solve(TPAPAVM, progress, localCTS.Token);

                    var position3 = new Position(solve3.Coordinates, Latitude, Longitude);

                    Logger.Info($"Third measurement point {solve3.Coordinates} - Vector: {position3.Vector}");

                    progress?.Report(GetStatus("Calculating Error"));

                    TPAPAVM.PolarErrorDetermination = new PolarErrorDetermination(solve3, position1, position2, position3, Latitude, Longitude);

                    Logger.Info($"Calculated Error: Az: { TPAPAVM.PolarErrorDetermination.InitialMountAxisAzimuthError}, Alt: { TPAPAVM.PolarErrorDetermination.InitialMountAxisAltitudeError}, Tot: { TPAPAVM.PolarErrorDetermination.InitialMountAxisTotalError}");

                    TPAPAVM.Step = TPAPAStep.Adjustment;

                    TPAPAVM.ArcsecPerPix = AstroUtil.ArcsecPerPixel(profileService.ActiveProfile.CameraSettings.PixelSize * Binning?.X ?? 1, profileService.ActiveProfile.TelescopeSettings.FocalLength);
                    var width = TPAPAVM.Image.Image.PixelWidth;
                    var height = TPAPAVM.Image.Image.PixelHeight;
                    TPAPAVM.Center = new Point(width / 2, height / 2);

                    await TPAPAVM.SelectNewReferenceStar(TPAPAVM.Center);

                    do {
                        var continuousSolve = await Solve(TPAPAVM, progress, localCTS.Token);
                        if (continuousSolve.Success) {
                            await TPAPAVM.UpdateDetails(continuousSolve);
                        }
                    } while (!localCTS.Token.IsCancellationRequested);

                    return true;
                }
            } catch (OperationCanceledException) {
            } catch (Exception ex) {
                throw;
            } finally {
                telescopeMediator.SetTrackingMode(Equipment.Interfaces.TrackingMode.Sidereal);
                telescopeMediator.SetTrackingEnabled(false);
                externalProgress?.Report(GetStatus(string.Empty));
                TPAPAVM.Step = string.Empty;
            }
            return false;
        }
        public bool Northern {
            get => profileService.ActiveProfile.AstrometrySettings.Latitude > 0;
        }

        /// <summary>
        /// Calculate the error based on the measured telescope axis compared to the polar axis
        /// Polar axis = Azimuth 0 | Altitude = Latitude
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        private (Angle, Angle) CalculateError(TopocentricCoordinates axis) {
            if (Northern) {
                var altError = axis.Altitude - Latitude;
                var azError = axis.Azimuth;
                return (altError, azError);
            } else {
                var altError = axis.Altitude + Latitude;
                var azError = axis.Azimuth + Angle.ByDegree(180);
                return (altError, azError);
            }
        }

        private FilterInfo filter;

        public FilterInfo Filter { get => filter; set { filter = value; RaisePropertyChanged(); } }

        private double exposureTime;

        public double ExposureTime {
            get => exposureTime; set { exposureTime = value; RaisePropertyChanged(); }
        }

        private int gain;

        public int Gain { get => gain; set { gain = value; RaisePropertyChanged(); } }

        private int offset;

        public int Offset { get => offset; set { offset = value; RaisePropertyChanged(); } }

        private BinningMode binning;

        public BinningMode Binning { get => binning; set { binning = value; RaisePropertyChanged(); } }

        private CameraInfo cameraInfo;

        public CameraInfo CameraInfo { get => cameraInfo; private set { cameraInfo = value; RaisePropertyChanged(); } }

        private async Task<PlateSolveResult> Solve(TPAPAVM TPAPAVM, IProgress<ApplicationStatus> progress, CancellationToken token) {
            PlateSolveResult result = new PlateSolveResult { Success = false };

            do {
                var solver = PlateSolverFactory.GetPlateSolver(profileService.ActiveProfile.PlateSolveSettings);

                var seq = new CaptureSequence() { Binning = Binning, Gain = Gain, ExposureTime = ExposureTime, Offset = Offset, FilterType = Filter };
                var image = await imagingMediator.CaptureAndPrepareImage(seq, new Core.Utility.PrepareImageParameters(true, false), token, progress);

                if (image != null) {
                    TPAPAVM.Image = image;

                    var imageSolver = new ImageSolver(solver, null);

                    var parameter = new PlateSolveParameter() {
                        Binning = Binning?.X ?? 1,
                        Coordinates = telescopeMediator.GetCurrentPosition(),
                        DownSampleFactor = profileService.ActiveProfile.PlateSolveSettings.DownSampleFactor,
                        FocalLength = profileService.ActiveProfile.TelescopeSettings.FocalLength,
                        MaxObjects = profileService.ActiveProfile.PlateSolveSettings.MaxObjects,
                        PixelSize = profileService.ActiveProfile.CameraSettings.PixelSize,
                        Regions = profileService.ActiveProfile.PlateSolveSettings.Regions,
                        SearchRadius = SearchRadius,
                        DisableNotifications = true
                    };

                    result = await imageSolver.Solve(image.RawImageData, parameter, progress, token);
                    if (!result.Success) {
                        await CoreUtil.Wait(TimeSpan.FromSeconds(1), token, progress, "Plate solve failed. Retrying...");
                    }
                } else {
                    await CoreUtil.Wait(TimeSpan.FromSeconds(1), token, progress, "Image capture failed. Retrying...");
                }
            } while (result.Success == false);
            return result;
        }

        private async Task MoveToNextPoint(IProgress<ApplicationStatus> progress, CancellationToken token) {
            try {
                var startPosition = telescopeMediator.GetCurrentPosition();
                var currentPosition = telescopeMediator.GetCurrentPosition();
                var rates = telescopeMediator.GetInfo().PrimaryAxisRates;

                var foundRate = rates
                    .OrderBy(x => x.Item2)
                    .Where(x => x.Item1 <= MoveRate && MoveRate <= x.Item2 || x.Item2 < MoveRate).LastOrDefault();

                var adjustedRate = MoveRate;
                if (foundRate.Item2 < MoveRate) {
                    Logger.Warning($"Provided MoveRate of {MoveRate} is not supported. Using {adjustedRate} instead");
                    //The closest rate is below the specified move rate as no move rate is found for the value
                    adjustedRate = foundRate.Item2;
                }

                Logger.Info($"Moving axis by {adjustedRate} into direction {(EastDirection ? "East" : "West")} until distance {TargetDistance}° is traveled");
                telescopeMediator.MoveAxis(Core.Enum.TelescopeAxes.Primary, EastDirection ? adjustedRate : -adjustedRate);

                //Move Rate is degree/s
                var timeToDestination = TimeSpan.FromSeconds(TargetDistance / adjustedRate);

                using (var cts = CancellationTokenSource.CreateLinkedTokenSource(token)) {
                    try {
                        cts.CancelAfter(timeToDestination);

                        while ((180 -  Math.Abs(Math.Abs(currentPosition.RADegrees - startPosition.RADegrees) - 180)) < TargetDistance) {
                            await Task.Delay(100, cts.Token);
                            currentPosition = telescopeMediator.GetCurrentPosition();

                            var distance = 180 - Math.Abs(Math.Abs(currentPosition.RADegrees - startPosition.RADegrees) - 180);

                            progress?.Report(new ApplicationStatus() { Status = "Moving to next point", MaxProgress = TargetDistance, Progress = distance, ProgressType = ApplicationStatus.StatusProgressType.ValueOfMaxValue });
                        }
                    } catch (OperationCanceledException ex) {
                        // Rethrow cancellation when parent token is cancelled
                        if (token.IsCancellationRequested) {
                            throw;
                        }
                    }
                }

                telescopeMediator.MoveAxis(Core.Enum.TelescopeAxes.Primary, 0);

                await CoreUtil.Wait(TimeSpan.FromSeconds(profileService.ActiveProfile.TelescopeSettings.SettleTime), token, progress, "Settling");
                telescopeMediator.SetTrackingMode(Equipment.Interfaces.TrackingMode.Sidereal);
                telescopeMediator.SetTrackingEnabled(true);

                progress?.Report(new ApplicationStatus() { Status = string.Empty });
            } catch (Exception ex) {
                //Reset move rate in case of problems or early cancellation
                telescopeMediator.MoveAxis(Core.Enum.TelescopeAxes.Primary, 0);
                throw;
            }
        }

        public Angle Latitude {
            get => Angle.ByDegree(profileService.ActiveProfile.AstrometrySettings.Latitude);
        }
        public Angle Longitude {
            get => Angle.ByDegree(profileService.ActiveProfile.AstrometrySettings.Longitude);
        }
    }
}