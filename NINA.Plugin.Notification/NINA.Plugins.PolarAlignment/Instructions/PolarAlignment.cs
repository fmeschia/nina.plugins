﻿using Newtonsoft.Json;
using NINA.Astrometry;
using NINA.Core.Locale;
using NINA.Core.Model;
using NINA.Core.Model.Equipment;
using NINA.Core.Utility;
using NINA.Core.Utility.WindowService;
using NINA.Equipment.Equipment.MyCamera;
using NINA.Equipment.Interfaces.Mediator;
using NINA.Equipment.Model;
using NINA.Image.ImageAnalysis;
using NINA.Image.Interfaces;
using NINA.PlateSolving;
using NINA.Profile.Interfaces;
using NINA.Sequencer.SequenceItem;
using NINA.Sequencer.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace NINA.Plugins.PolarAlignment.Instructions {
    /// <summary>
    /// This Item shows the basic principle on how to add a new Sequence Item to the N.I.N.A. sequencer via the plugin interface
    /// For ease of use this item inherits the abstract SequenceItem which already handles most of the running logic, like logging, exception handling etc.
    /// A complete custom implementation by just implementing ISequenceItem is possible too
    /// The following MetaData can be set to drive the initial values
    /// --> Name - The name that will be displayed for the item
    /// --> Description - a brief summary of what the item is doing. It will be displayed as a tooltip on mouseover in the application
    /// --> Icon - a string to the key value of a Geometry inside N.I.N.A.'s geometry resources
    ///
    /// If the item has some preconditions that should be validated, it shall also extend the IValidatable interface and add the validation logic accordingly.
    /// </summary>
    [ExportMetadata("Name", "Three Point Polar Alignment")]
    [ExportMetadata("Description", "Three Position Auto Polar Alignment anywhere in the sky")]
    [ExportMetadata("Icon", "ThreePointsSVG")]
    [ExportMetadata("Category", "Polar Alignment")]
    [Export(typeof(ISequenceItem))]
    [JsonObject(MemberSerialization.OptIn)]
    public class PolarAlignment : SequenceItem, IValidatable {
        private IProfileService profileService;
        private ICameraMediator cameraMediator;
        private IImagingMediator imagingMediator;
        private IFilterWheelMediator fwMediator;
        private ITelescopeMediator telescopeMediator;
        private double moveRate;
        private double searchRadius;
        private int targetDistance;
        private bool eastDirection;
        private IList<string> issues = new List<string>();

        [ImportingConstructor]
        public PolarAlignment(IProfileService profileService, ICameraMediator cameraMediator, IImagingMediator imagingMediator, IFilterWheelMediator fwMediator, ITelescopeMediator telescopeMediator) {
            this.profileService = profileService;
            this.cameraMediator = cameraMediator;
            this.imagingMediator = imagingMediator;
            this.fwMediator = fwMediator;
            this.telescopeMediator = telescopeMediator;

            Gain = profileService.ActiveProfile.PlateSolveSettings.Gain;
            Offset = -1;
            ExposureTime = profileService.ActiveProfile.PlateSolveSettings.ExposureTime;
            Binning = new BinningMode(profileService.ActiveProfile.PlateSolveSettings.Binning, profileService.ActiveProfile.PlateSolveSettings.Binning);

            EastDirection = Properties.Settings.Default.DefaultEastDirection;
            MoveRate = Properties.Settings.Default.DefaultMoveRate;
            TargetDistance = Properties.Settings.Default.DefaultTargetDistance;
            SearchRadius = Properties.Settings.Default.DefaultSearchRadius;

            CameraInfo = this.cameraMediator.GetInfo();

            if (Northern) {
                Coordinates = new InputTopocentricCoordinates(new TopocentricCoordinates(Angle.ByDegree(Properties.Settings.Default.DefaultAzimuthOffset), Latitude + Angle.ByDegree(Properties.Settings.Default.DefaultAltitudeOffset), Latitude, Longitude));
            } else {
                Coordinates = new InputTopocentricCoordinates(new TopocentricCoordinates(Angle.ByDegree(Properties.Settings.Default.DefaultAzimuthOffset), Angle.ByDegree(Math.Abs(Latitude.Degree)) + Angle.ByDegree(Properties.Settings.Default.DefaultAltitudeOffset), Latitude, Longitude));
            }
        }

        private PolarAlignment(PolarAlignment copyMe): this(copyMe.profileService, copyMe.cameraMediator, copyMe.imagingMediator, copyMe.fwMediator, copyMe.telescopeMediator) {
            CopyMetaData(copyMe);
        }

        /// <summary>
        /// When items are put into the sequence via the factory, the factory will call the clone method. Make sure all the relevant fields are cloned with the object.
        /// </summary>
        /// <returns></returns>
        public override object Clone() {
            var clone = new PolarAlignment(this) {
                MoveRate = MoveRate,
                TargetDistance = TargetDistance,
                EastDirection = EastDirection,
                ExposureTime = ExposureTime,
                Binning = Binning,
                Gain = Gain,
                Offset = Offset,
                Coordinates = new InputTopocentricCoordinates(Coordinates.Coordinates.Copy()),
            };

            if (clone.Binning == null) {
                clone.Binning = new BinningMode(1, 1);
            }

            return clone;
        }

        [JsonProperty]
        public InputTopocentricCoordinates Coordinates { get; set; }

        [JsonProperty]
        public double MoveRate {
            get => moveRate;
            set {
                moveRate = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty]
        public double SearchRadius {
            get => searchRadius;
            set {
                searchRadius = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty]
        public bool EastDirection {
            get => eastDirection;
            set {
                eastDirection = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty]
        public int TargetDistance {
            get => targetDistance;
            set {
                targetDistance = value;
                RaisePropertyChanged();
            }
        }

        //public IWindowServiceFactory WindowServiceFactory { get; set; } = new WindowServiceFactory();

        private ApplicationStatus GetStatus(string status) {
            return new ApplicationStatus { Source = "Polar Alignment", Status = status };
        }

        /// <summary>
        /// The core logic when the sequence item is running resides here
        /// Add whatever action is necessary
        /// </summary>
        /// <param name="progress">The application status progress that can be sent back during execution</param>
        /// <param name="token">When a cancel signal is triggered from outside, this token can be used to register to it or check if it is cancelled</param>
        /// <returns></returns>
        public override async Task Execute(IProgress<ApplicationStatus> externalProgress, CancellationToken token) {
            var service = new CustomWindowService();
            try {
                using (var localCTS = CancellationTokenSource.CreateLinkedTokenSource(token)) {
                    var context = new TPAPAVM(profileService);
                    IProgress<ApplicationStatus> progress = new Progress<ApplicationStatus>(p => { context.Status = p; externalProgress?.Report(p); });

                    service.Show(context, Loc.Instance["LblPolarAlignment"], System.Windows.ResizeMode.CanResizeWithGrip, System.Windows.WindowStyle.SingleBorderWindow);
                    service.OnClosed += (s, e) => {
                        try {
                            localCTS?.Cancel();
                        } catch (Exception) { }
                    };

                    context.Step = TPAPAStep.MovingToTarget;
                    Logger.Info($"Slewing to initial position {Coordinates.Coordinates}");
                    await telescopeMediator.SlewToCoordinatesAsync(Coordinates.Coordinates, localCTS.Token);
                    telescopeMediator.SetTrackingEnabled(true);

                    context.Step = TPAPAStep.FirstPoint;

                    var solve1 = await Solve(context, progress, localCTS.Token);

                    var position1 = new Position(solve1.Coordinates, Latitude, Longitude);

                    Logger.Info($"First measurement point {solve1.Coordinates} - Vector: {position1.Vector}");

                    context.Step = TPAPAStep.SecondPoint;

                    await MoveToNextPoint(progress, localCTS.Token);
                    var solve2 = await Solve(context, progress, localCTS.Token);

                    var position2 = new Position(solve2.Coordinates, Latitude, Longitude);

                    Logger.Info($"Second measurement point {solve2.Coordinates} - Vector: {position2.Vector}");

                    context.Step = TPAPAStep.FinalPoint;

                    await MoveToNextPoint(progress, localCTS.Token);
                    var solve3 = await Solve(context, progress, localCTS.Token);

                    var position3 = new Position(solve3.Coordinates, Latitude, Longitude);

                    Logger.Info($"Third measurement point {solve3.Coordinates} - Vector: {position3.Vector}");

                    progress?.Report(GetStatus("Calculating Error"));


                    context.PolarErrorDetermination = new PolarErrorDetermination(solve3, position1, position2, position3, Latitude, Longitude);

                    Logger.Info($"Calculated Error: Az: { context.PolarErrorDetermination.InitialMountAxisAzimuthError}, Alt: { context.PolarErrorDetermination.InitialMountAxisAltitudeError}, Tot: { context.PolarErrorDetermination.InitialMountAxisTotalError}");

                    context.Step = TPAPAStep.Adjustment;                   

                    context.ArcsecPerPix = AstroUtil.ArcsecPerPixel(profileService.ActiveProfile.CameraSettings.PixelSize * Binning?.X ?? 1, profileService.ActiveProfile.TelescopeSettings.FocalLength);
                    var width = context.Image.Image.PixelWidth;
                    var height = context.Image.Image.PixelHeight;
                    context.Center = new Point(width / 2, height / 2);

                    await context.SelectNewReferenceStar(context.Center);


                    do {
                        var continuousSolve = await Solve(context, progress, localCTS.Token);
                        if (continuousSolve.Success) {


                            await context.UpdateDetails(continuousSolve);
                        }
                    } while (!localCTS.Token.IsCancellationRequested);

                    await service.Close();
                    return;
                }
            } catch (OperationCanceledException) {
            } catch (Exception ex) {
                await service?.Close();
                throw;
            } finally {
                externalProgress?.Report(GetStatus(string.Empty));
                telescopeMediator.SetTrackingMode(Equipment.Interfaces.TrackingMode.Sidereal);
                telescopeMediator.SetTrackingEnabled(false);
            }
        }

        public bool Northern {
            get => profileService.ActiveProfile.AstrometrySettings.Latitude > 0;
        }

        /// <summary>
        /// Calculate the error based on the measured telescope axis compared to the polar axis
        /// Polar axis = Azimuth 0 | Altitude = Latitude
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        private (Angle, Angle) CalculateError(TopocentricCoordinates axis) {
            if (Northern) {
                var altError = axis.Altitude - Latitude;
                var azError = axis.Azimuth;
                return (altError, azError);
            } else {
                var altError = axis.Altitude + Latitude;
                var azError = axis.Azimuth + Angle.ByDegree(180);
                return (altError, azError);
            }
        }

        private FilterInfo filter;

        [JsonProperty]
        public FilterInfo Filter { get => filter; set { filter = value; RaisePropertyChanged(); } }

        private double exposureTime;

        [JsonProperty]
        public double ExposureTime {
            get => exposureTime; set { exposureTime = value; RaisePropertyChanged(); }
        }

        private int gain;

        [JsonProperty]
        public int Gain { get => gain; set { gain = value; RaisePropertyChanged(); } }

        private int offset;

        [JsonProperty]
        public int Offset { get => offset; set { offset = value; RaisePropertyChanged(); } }

        private BinningMode binning;

        [JsonProperty]
        public BinningMode Binning { get => binning; set { binning = value; RaisePropertyChanged(); } }

        private CameraInfo cameraInfo;

        public CameraInfo CameraInfo { get => cameraInfo; private set { cameraInfo = value; RaisePropertyChanged(); } }

        private async Task<PlateSolveResult> Solve(TPAPAVM context, IProgress<ApplicationStatus> progress, CancellationToken token) {
            PlateSolveResult result = new PlateSolveResult { Success = false };

            do {
                var solver = PlateSolverFactory.GetPlateSolver(profileService.ActiveProfile.PlateSolveSettings);

                var seq = new CaptureSequence() { Binning = Binning, Gain = Gain, ExposureTime = ExposureTime, Offset = Offset, FilterType = Filter };
                var image = await imagingMediator.CaptureAndPrepareImage(seq, new Core.Utility.PrepareImageParameters(true, false), token, progress);

                if (image != null) {
                    context.Image = image;

                    var imageSolver = new ImageSolver(solver, null);

                    var parameter = new PlateSolveParameter() {
                        Binning = Binning?.X ?? 1,
                        Coordinates = telescopeMediator.GetCurrentPosition(),
                        DownSampleFactor = profileService.ActiveProfile.PlateSolveSettings.DownSampleFactor,
                        FocalLength = profileService.ActiveProfile.TelescopeSettings.FocalLength,
                        MaxObjects = profileService.ActiveProfile.PlateSolveSettings.MaxObjects,
                        PixelSize = profileService.ActiveProfile.CameraSettings.PixelSize,
                        Regions = profileService.ActiveProfile.PlateSolveSettings.Regions,
                        SearchRadius = SearchRadius,
                        DisableNotifications = true
                    };

                    result = await imageSolver.Solve(image.RawImageData, parameter, progress, token);
                    if (!result.Success) {
                        await CoreUtil.Wait(TimeSpan.FromSeconds(1), token, progress, "Plate solve failed. Retrying...");
                    }
                } else {
                    await CoreUtil.Wait(TimeSpan.FromSeconds(1), token, progress, "Image capture failed. Retrying...");
                }
            } while (result.Success == false);
            return result;
        }

        private async Task MoveToNextPoint(IProgress<ApplicationStatus> progress, CancellationToken token) {
            try {
                var startPosition = telescopeMediator.GetCurrentPosition();
                var currentPosition = telescopeMediator.GetCurrentPosition();
                var rates = telescopeMediator.GetInfo().PrimaryAxisRates;

                var foundRate = rates
                    .OrderBy(x => x.Item2)
                    .Where(x => x.Item1 <= MoveRate && MoveRate <= x.Item2 || x.Item2 < MoveRate).LastOrDefault();

                var adjustedRate = MoveRate;
                if(foundRate.Item2 < MoveRate) {
                    Logger.Warning($"Provided MoveRate of {MoveRate} is not supported. Using {adjustedRate} instead");
                    //The closest rate is below the specified move rate as no move rate is found for the value
                    adjustedRate = foundRate.Item2;
                }

                Logger.Info($"Moving axis by {adjustedRate} into direction {(EastDirection ? "East" : "West")} until distance {TargetDistance}° is traveled");
                telescopeMediator.MoveAxis(Core.Enum.TelescopeAxes.Primary, EastDirection ? adjustedRate : -adjustedRate);

                //Move Rate is degree/s
                var timeToDestination = TimeSpan.FromSeconds(TargetDistance / adjustedRate);

                using (var cts = CancellationTokenSource.CreateLinkedTokenSource(token)) {
                    try {
                        cts.CancelAfter(timeToDestination);                        

                        while ((180 - Math.Abs(Math.Abs(currentPosition.RADegrees - startPosition.RADegrees) - 180)) < TargetDistance) {
                            await Task.Delay(100, cts.Token);
                            currentPosition = telescopeMediator.GetCurrentPosition();

                            var distance = 180 - Math.Abs(Math.Abs(currentPosition.RADegrees - startPosition.RADegrees) - 180);

                            progress?.Report(new ApplicationStatus() { Status = "Moving to next point", MaxProgress = TargetDistance, Progress = distance, ProgressType = ApplicationStatus.StatusProgressType.ValueOfMaxValue });
                        }
                    } catch (OperationCanceledException ex) {
                        // Rethrow cancellation when parent token is cancelled
                        if (token.IsCancellationRequested) {
                            throw;
                        }
                    }
                }


                telescopeMediator.MoveAxis(Core.Enum.TelescopeAxes.Primary, 0);

                await CoreUtil.Wait(TimeSpan.FromSeconds(profileService.ActiveProfile.TelescopeSettings.SettleTime), token, progress, "Settling");
                telescopeMediator.SetTrackingMode(Equipment.Interfaces.TrackingMode.Sidereal);
                telescopeMediator.SetTrackingEnabled(true);

                progress?.Report(new ApplicationStatus() { Status = string.Empty });
            } catch (Exception ex) {
                //Reset move rate in case of problems or early cancellation
                telescopeMediator.MoveAxis(Core.Enum.TelescopeAxes.Primary, 0);
                throw;
            }
        }

        public Angle Latitude {
            get => Angle.ByDegree(profileService.ActiveProfile.AstrometrySettings.Latitude);
        }
        public Angle Longitude {
            get => Angle.ByDegree(profileService.ActiveProfile.AstrometrySettings.Longitude);
        }

        /// <summary>
        /// This string will be used for logging
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return $"Category: {Category}, Item: {nameof(PolarAlignment)}, Rate: {MoveRate}, Distance: {TargetDistance}";
        }

        public IList<string> Issues { get => issues; set { issues = value; RaisePropertyChanged(); } }

        public bool Validate() {
            var i = new List<string>();

            //Camera
            CameraInfo = this.cameraMediator.GetInfo();
            if (!CameraInfo.Connected) {
                i.Add(Loc.Instance["LblCameraNotConnected"]);
            } else {
                if (CameraInfo.CanSetGain && Gain > -1 && (Gain < CameraInfo.GainMin || Gain > CameraInfo.GainMax)) {
                    i.Add(string.Format(Loc.Instance["Lbl_SequenceItem_Imaging_TakeExposure_Validation_Gain"], CameraInfo.GainMin, CameraInfo.GainMax, Gain));
                }
                if (CameraInfo.CanSetOffset && Offset > -1 && (Offset < CameraInfo.OffsetMin || Offset > CameraInfo.OffsetMax)) {
                    i.Add(string.Format(Loc.Instance["Lbl_SequenceItem_Imaging_TakeExposure_Validation_Offset"], CameraInfo.OffsetMin, CameraInfo.OffsetMax, Offset));
                }
            }

            //Filter wheel
            if (filter != null && !fwMediator.GetInfo().Connected) {
                i.Add(Loc.Instance["LblFilterWheelNotConnected"]);
            }

            //Mount
            if (!telescopeMediator.GetInfo().Connected) {
                i.Add(Loc.Instance["LblTelescopeNotConnected"]);
            } else if (!telescopeMediator.GetInfo().CanMovePrimaryAxis) {
                i.Add("Telescope cannot move primary axis. This is required for the automated slews around the right ascension axis.");
            }

            //Solver
            if (profileService.ActiveProfile.PlateSolveSettings.PlateSolverType == Core.Enum.PlateSolverEnum.ASTROMETRY_NET) {
                i.Add("Astrometry.net is too slow for this method to work properly. Please choose a different solver.");
            }

            Issues = i;
            return i.Count == 0;
        }

        public class CustomWindowService : IWindowService {
            protected Dispatcher dispatcher = Application.Current?.Dispatcher ?? Dispatcher.CurrentDispatcher;
            protected CustomWindow window;

            public void Show(object content, string title = "", ResizeMode resizeMode = ResizeMode.NoResize, WindowStyle windowStyle = WindowStyle.None) {
                dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => {
                    window = new CustomWindow() {
                        SizeToContent = SizeToContent.Manual,
                        Title = title,
                        Background = Application.Current.TryFindResource("BackgroundBrush") as Brush,
                        ResizeMode = resizeMode,
                        WindowStyle = windowStyle,
                        MinHeight = 600,
                        MinWidth = 600,
                        Style = Application.Current.TryFindResource("NoResizeWindow") as Style,
                    };
                    window.CloseCommand = new RelayCommand((object o) => window.Close());
                    window.Closed += (object sender, EventArgs e) => this.OnClosed?.Invoke(this, null);
                    window.ContentRendered += (object sender, EventArgs e) => window.InvalidateVisual();
                    window.Content = content;
                    window.Owner = Application.Current.MainWindow;
                    window.Show();
                }));
            }

            public void DelayedClose(TimeSpan t) {
                Task.Run(async () => {
                    await CoreUtil.Wait(t);
                    await this.Close();
                });
            }

            public async Task Close() {
                await dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => {
                    window?.Close();
                }));
            }

            public IDispatcherOperationWrapper ShowDialog(object content, string title = "", ResizeMode resizeMode = ResizeMode.NoResize, WindowStyle windowStyle = WindowStyle.None, ICommand closeCommand = null) {
                return new DispatcherOperationWrapper(dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => {
                    window = new CustomWindow() {
                        SizeToContent = SizeToContent.WidthAndHeight,
                        Title = title,
                        Background = Application.Current.TryFindResource("BackgroundBrush") as Brush,
                        ResizeMode = resizeMode,
                        WindowStyle = windowStyle,
                        Style = Application.Current.TryFindResource("NoResizeWindow") as Style,
                    };
                    if (closeCommand == null) {
                        window.CloseCommand = new RelayCommand((object o) => { window.Close(); Application.Current.MainWindow.Focus(); });
                    } else {
                        window.CloseCommand = closeCommand;
                    }
                    window.Closed += (object sender, EventArgs e) => this.OnClosed?.Invoke(this, null);
                    window.ContentRendered += (object sender, EventArgs e) => window.InvalidateVisual();

                    window.SizeChanged += Win_SizeChanged;
                    window.Content = content;
                    var mainwindow = System.Windows.Application.Current.MainWindow;
                    mainwindow.Opacity = 0.8;
                    window.Owner = Application.Current.MainWindow;
                    var result = window.ShowDialog();
                    this.OnDialogResultChanged?.Invoke(this, new DialogResultEventArgs(result));
                    mainwindow.Opacity = 1;
                })));
            }

            public event EventHandler OnDialogResultChanged;

            public event EventHandler OnClosed;

            private static void Win_SizeChanged(object sender, SizeChangedEventArgs e) {
                var mainwindow = System.Windows.Application.Current.MainWindow;
                var win = (System.Windows.Window)sender;
                win.Left = mainwindow.Left + (mainwindow.Width - win.ActualWidth) / 2; ;
                win.Top = mainwindow.Top + (mainwindow.Height - win.ActualHeight) / 2;
            }
        }
    }
}