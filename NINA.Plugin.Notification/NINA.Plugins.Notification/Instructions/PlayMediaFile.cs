﻿using Newtonsoft.Json;
using NINA.Model;
using NINA.Sequencer.SequenceItem;
using NINA.Sequencer.Validations;
using NINA.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace NINA.Plugins.Notification.Instructions {
    [ExportMetadata("Name", "Play Media File")]
    [ExportMetadata("Description", "This item will play the specified media file")]
    [ExportMetadata("Icon", "NINA.Plugins.Notification.Instructions.PlayMediaFileSVG")]
    [ExportMetadata("Category", "Notification")]
    [Export(typeof(ISequenceItem))]
    [JsonObject(MemberSerialization.OptIn)]
    public class PlayMediaFile : SequenceItem, IValidatable {
        private string source;
        private MediaPlayer mediaPlayer;
        private DispatcherTimer timer = new DispatcherTimer();
        private IList<string> issues = new List<string>();
        private TimeSpan totalPlayTime;
        private TimeSpan currentPlayTime;
        private bool isPlaying;

        [ImportingConstructor]
        public PlayMediaFile() {            
            mediaPlayer = new MediaPlayer();
            timer = new DispatcherTimer();
            Source = Properties.Settings.Default.DefaultSoundNotificationPath;
        }

        [JsonProperty]
        public string Source {
            get => source;
            set {
                source = value;
                RaisePropertyChanged();
            }
        }

        public bool IsPlaying {
            get => isPlaying;
            set {
                isPlaying = value;
                RaisePropertyChanged();
            }
        }

        public TimeSpan TotalPlayTime { 
            get => totalPlayTime; 
            private set {
                totalPlayTime = value;
                RaisePropertyChanged();
            }
        }
        public TimeSpan CurrentPlayTime {
            get => currentPlayTime;
            private set {
                currentPlayTime = value;
                RaisePropertyChanged();
            }
        }

        public IList<string> Issues {
            get => issues;
            set {
                issues = value;
                RaisePropertyChanged();
            }
        }

        public override object Clone() {
            return new PlayMediaFile() {
                Icon = Icon,
                Name = Name,
                Category = Category,
                Description = Description,
                Source = string.IsNullOrEmpty(Source) ? Properties.Settings.Default.DefaultSoundNotificationPath : Source
            };
        }

        public override async Task Execute(IProgress<ApplicationStatus> progress, CancellationToken token) {
            timer.Stop();
            await mediaPlayer.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => {
                mediaPlayer.Close();
                mediaPlayer.Open(new Uri(Source));                
                mediaPlayer.MediaOpened += MediaPlayer_MediaOpened;
            }));
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += new EventHandler(ticktock);
            timer.Start();

            while(!IsPlaying) {
                await Task.Delay(1000, token);
            }            

            cts = CancellationTokenSource.CreateLinkedTokenSource(token);
            using (cts.Token.Register(() => {
                mediaPlayer.Stop();
                IsPlaying = false;
                mediaPlayer.MediaOpened -= MediaPlayer_MediaOpened;
                mediaPlayer.MediaEnded -= MediaPlayer_MediaEnded;
            })) { 
                await Utility.Utility.Wait(TotalPlayTime, cts.Token, progress, "Playing Media File");
            }

        }

        private CancellationTokenSource cts;

        private void MediaPlayer_MediaEnded(object sender, EventArgs e) {
            IsPlaying = false;
        }

        private void MediaPlayer_MediaOpened(object sender, EventArgs e) {
            TotalPlayTime = mediaPlayer.NaturalDuration.TimeSpan;
            mediaPlayer.Position = TimeSpan.Zero;
            mediaPlayer.Play();
            IsPlaying = true;
            mediaPlayer.MediaEnded += MediaPlayer_MediaEnded;
        }

        private void ticktock(object sender, EventArgs e) {
            CurrentPlayTime = mediaPlayer.Position;
        }

        public bool Validate() {
            var i = new List<string>();
            if(!File.Exists(Source)) {
                i.Add($"File does not exist {Source}");
            }
            Issues = i;
            return i.Count == 0;
        }
    }
}
