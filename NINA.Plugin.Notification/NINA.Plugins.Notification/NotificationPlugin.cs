﻿using NINA.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NINA.Plugins.Notification {
    [Export(typeof(IPlugin))]
    class NotificationPlugin : IPlugin {
        [ImportingConstructor]
        public NotificationPlugin() {
        }
        public string Name => "NINA.Plugins.Notifications";
        public string Description => "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

        public string DefaultSoundNotificationPath {
            get {
                return Properties.Settings.Default.DefaultSoundNotificationPath;
            }
            set {
                Properties.Settings.Default.DefaultSoundNotificationPath = value;
                Properties.Settings.Default.Save();
            }
        }
    }
}